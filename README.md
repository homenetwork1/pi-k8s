# pi k8s

The helm charts for my personal local k8s cluster running on raspberry pi's

## project setup

[install helm](https://helm.sh/docs/intro/install/)

add all helm repos

```bash
helm repo add traefik https://helm.traefik.io/traefik
helm repo add argo https://argoproj.github.io/argo-helm
helm repo add jetstack https://charts.jetstack.io
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
```
