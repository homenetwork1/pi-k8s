
traefik:
	helm upgrade --install traefik ./charts/traefik  -n kube-system
traefik-template:
	helm template traefik ./charts/traefik  -n kube-system

mariadb:
	helm upgrade --install mariadb ./charts/mariadb

whoami:
	helm upgrade --install whoami ./charts/whoami
sealed-secret:
	helm upgrade --install sealed-secret ./charts/sealed-secrets --create-namespace -n kube-system

calibre:
	helm upgrade --install calibre ./charts/calibre --create-namespace -n calibre

monitoring:
	helm upgrade --install monitoring ./charts/monitoring --create-namespace -n monitoring
monitoring-forward-grafana:
	kubectl port-forward service/monitor-application-grafana 8800:80 -n monitoring
monitoring-forward-prometheus:
	kubectl port-forward service/monitor-application-prometheus-server 8900:80 -n monitoring

cloudflare:
	helm upgrade --install cloudflare-tunnel ./charts/cloudflare-tunnel --create-namespace -n cloudflare
cloudflare-template:
	helm template cloudflare-tunnel ./charts/cloudflare-tunnel --create-namespace -n cloudflare

argo-cd:
	helm upgrade --install argo-cd ./charts/argo-cd --create-namespace -n argo-cd
argo-cd-template:
	helm template argo-cd ./charts/argo-cd -n argo-cd --debug

cert-manager:
	helm upgrade --install cert-manager ./charts/cert-manager --create-namespace -n cert-manager
cert-manager-template:
	helm template cert-manager ./charts/cert-manager -n cert-manager --debug

gitlab-runners:
	helm upgrade --install gitlab-runners ./charts/gitlab-runners --create-namespace -n gitlab-runners
gitlab-runners-template:
	helm template gitlab-runners ./charts/gitlab-runners -n gitlab-runners --debug


postgres:
	helm upgrade --install postgres ./charts/postgres --create-namespace -n postgres
postgres-template:
	helm template postgres ./charts/postgres -n postgres --debug
postgres-forward-port:
	kubectl port-forward service/postgresql-application 25432:5432 -n postgres

web-libro-template:
	helm template web-libro ./charts/web-libro -n web-libro --debug
exec-web-libro:
	kubectl exec -it deploy/web-libro-application -n web-libro -- bash
logs-web-libro:
	kubectl logs deploy/web-libro-application -n web-libro

