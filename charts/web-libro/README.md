# documentation

## create sealed secret

### jwt secrets

```bash
ssh-keygen -t rsa -b 4096 -m PEM -f jwtRS256.key
# Don't add passphrase
openssl rsa -in jwtRS256.key -pubout -outform PEM -out jwtRS256.key.pub

kubectl create secret generic jwt-rs256-keys \
  --from-file=key="jwtRS256.key" \
  --from-file=pub="jwtRS256.key.pub" \
  --dry-run=client \
  -n web-libro \
  -o yaml > temp_runner_secret.yaml


kubeseal < temp_runner_secret.yaml -o yaml >./charts/web-libro/templates/jwt-sealedsecret.yaml

```

### mail secrets

```bash
kubectl create secret generic web-libros-mail-secrets \
  --from-literal=username='bar' \
  --from-literal=password='foo' \
  --dry-run=client \
  -n web-libro \
  -o yaml > temp_runner_secret.yaml

kubeseal < temp_runner_secret.yaml -o yaml >./charts/web-libro/templates/mail-sealedsecret.yaml
```

### database secrets

```bash
CREATE DATABASE yourdbname;
CREATE USER youruser WITH ENCRYPTED PASSWORD 'yourpass';
GRANT ALL PRIVILEGES ON DATABASE yourdbname TO youruser;
GRANT pg_read_all_data TO my_user;
GRANT pg_write_all_data TO my_user;
```

```bash
kubectl create secret generic web-libros-db-secrets \
  --from-literal=db_dsn='bar' \
  --dry-run=client \
  -n web-libro \
  -o yaml > temp_runner_secret.yaml

kubeseal < temp_runner_secret.yaml -o yaml >./charts/web-libro/templates/db-sealedsecret.yaml
```
