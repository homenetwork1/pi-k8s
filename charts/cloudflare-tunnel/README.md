# cloudflare tunnel

## setup

step1 - login

```
cloudflared login
```

step2 - create tunnel

```
cloudflared tunnel create pi-k3s-tunnel

```

store the tunnel id in the values

step3 - kube secret

By default, the credentials file will be created under ~/.cloudflared/<tunnel ID>.json
when you run `cloudflared tunnel create`. You can move it into a secret by using:

```
kubectl create secret generic tunnel-credentials \
  --from-file=credentials.json=~/.cloudflared/<tunnel ID>.json \
  --dry-run=client \
  -n cloudflare \
  -o yaml > temp_cloudflare_secret.yaml

kubeseal < temp_cloudflare_secret.yaml -o yaml >cloudflare-sealedsecret.yaml
```

step 4 dns record

```
cloudflared tunnel route dns pi-k3s-tunnel ebooks.van.denabeele.com
```
