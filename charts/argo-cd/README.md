# documentation

## create sealed secret

```bash

kubectl create secret generic gitlab-application-secrets \
  --from-literal=application-id=<token> \
  --from-literal=client_secret=<token> \
  --dry-run=client \
  -n argo-cd \
  -o yaml > temp_gitlab_app_secret.yaml

kubeseal < temp_gitlab_app_secret.yaml -o yaml > ./charts/argo-cd/templates/gitlab-application-sealedsecret.yaml
```

## create argo-cd cicd token

```bash
argocd login <url: example.com> --sso
argocd account generate-token --account <account-name>
```
