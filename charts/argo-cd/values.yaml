ingressRoute:
  rule: "Host(`argocd-van.denabeele.com`)"
  serviceName: argo-cd-argocd-server
  servicePort: http

argo-cd:
  redis:
    enabled: true
  redis-ha:
    enabled: false

  controller:
    replicas: 1
    nodeSelector:
      node-role: "agent"

  repoServer:
    replicas: 1
    nodeSelector:
      node-role: "agent"

  applicationSet:
    replicas: 1
    nodeSelector:
      node-role: "agent"

  dex:
    env:
      - name: GITLAB_APPLICATION_ID
        valueFrom:
          secretKeyRef:
            name: gitlab-application-secrets
            key: application-id
      - name: GITLAB_CLIENT_SECRET
        valueFrom:
          secretKeyRef:
            name: gitlab-application-secrets
            key: client_secret

    nodeSelector:
      node-role: "agent"

  server:
    replicas: 1
    extraArgs:
      - "--insecure"

    nodeSelector:
      node-role: "agent"

    certificate:
      # -- Deploy a Certificate resource (requires cert-manager)
      enabled: true
      # -- Certificate primary domain (commonName)
      domain: argocd-van.denabeele.com
      # -- The requested 'duration' (i.e. lifetime) of the Certificate. Value must be in units accepted by Go time.ParseDuration
      duration: ""
      # -- How long before the currently issued certificate's expiry cert-manager should renew the certificate. Value must be in units accepted by Go time.ParseDuration
      renewBefore: ""
      issuer:
        # -- Certificate issuer group. Set if using an external issuer. Eg. `cert-manager.io`
        group: ""
        # -- Certificate issuer kind. Either `Issuer` or `ClusterIssuer`
        kind: ClusterIssuer
        # -- Certificate isser name. Eg. `letsencrypt`
        name: cloudflare-issuer-production
      # -- Certificate manager additional hosts
      additionalHosts: []
      # -- The name of the Secret that will be automatically created and managed by this Certificate resource
      secretName: argocd-server-tls
  redis:
    enabled: true
    nodeSelector:
      node-role: "agent"
    resources: {}
    #  limits:
    #    cpu: 200m
    #    memory: 128Mi
    #  requests:
    #    cpu: 100m
    #    memory: 64Mi

  notifications:
    enabled: true
    nodeSelector:
      node-role: "agent"

  configs:
    cm:
      # Argo CD's externally facing base URL (optional). Required when configuring SSO
      url: "https://argocd-van.denabeele.com"
      # Argo CD instance label key
      application.instanceLabelKey: argocd.argoproj.io/instance

      # Enable logs RBAC enforcement
      # Ref: https://argo-cd.readthedocs.io/en/latest/operator-manual/upgrading/2.3-2.4/#enable-logs-rbac-enforcement
      server.rbac.log.enforce.enable: "false"

      # exec.enabled indicates whether the UI exec feature is enabled. It is disabled by default.
      # Ref: https://argo-cd.readthedocs.io/en/latest/operator-manual/rbac/#exec-resource
      exec.enabled: "false"

      # admin.enabled indicates whether the admin user is enabled. It is enabled by default.
      # https://argo-cd.readthedocs.io/en/latest/faq/#how-to-disable-admin-user
      admin.enabled: "true"

      accounts.account-weblibro-cicd: apiKey
      accounts.account-weblibro-cicd.enabled: "true"

      dex.config: |
        connectors:
          - type: gitlab
            # Required field for connector id.
            id: gitlab
            # Required field for connector name.
            name: GitLab
            config:
              # optional, default = https://gitlab.com
              baseURL: https://gitlab.com
              # Credentials can be string literals or pulled from the environment.
              clientID: $GITLAB_APPLICATION_ID
              clientSecret: $GITLAB_CLIENT_SECRET
              redirectURI: http://127.0.0.1:5556/dex/callback
              # Optional groups whitelist, communicated through the "groups" scope.
              # If `groups` is omitted, all of the user's GitLab groups are returned when the groups scope is present.
              # If `groups` is provided, this acts as a whitelist - only the user's GitLab groups that are in the configured `groups` below will go into the groups claim.  Conversely, if the user is not in any of the configured `groups`, the user will not be authenticated.
              groups:
              - argocd-access-group
              # flag which will switch from using the internal GitLab id to the users handle (@mention) as the user id.
              # It is possible for a user to change their own user name but it is very rare for them to do so
              useLoginAsID: false

    rbac:
      create: true
      policy.default: role:readonly
      policy.csv: |
        p, role:readonly, applications, get, */*, allow
        p, role:readonly, certificates, get, *, allow
        p, role:readonly, clusters, get, *, allow
        p, role:readonly, repositories, get, *, allow
        p, role:readonly, projects, get, *, allow
        p, role:readonly, accounts, get, *, allow
        p, role:readonly, gpgkeys, get, *, allow
        p, role:readonly, logs, get, */*, allow

        p, role:cicd, applications, sync, */*, allow

        p, role:admin, applications, create, */*, allow
        p, role:admin, applications, update, */*, allow
        p, role:admin, applications, delete, */*, allow
        p, role:admin, applications, sync, */*, allow
        p, role:admin, applications, override, */*, allow
        p, role:admin, applications, action/*, */*, allow
        p, role:admin, certificates, create, *, allow
        p, role:admin, certificates, update, *, allow
        p, role:admin, certificates, delete, *, allow
        p, role:admin, clusters, create, *, allow
        p, role:admin, clusters, update, *, allow
        p, role:admin, clusters, delete, *, allow
        p, role:admin, repositories, create, *, allow
        p, role:admin, repositories, update, *, allow
        p, role:admin, repositories, delete, *, allow
        p, role:admin, projects, create, *, allow
        p, role:admin, projects, update, *, allow
        p, role:admin, projects, delete, *, allow
        p, role:admin, accounts, update, *, allow
        p, role:admin, gpgkeys, create, *, allow
        p, role:admin, gpgkeys, delete, *, allow
        p, role:admin, exec, create, */*, allow

        g, role:admin, role:readonly
        g, admin, role:admin
        g, account-weblibro-cicd, role:cicd

        g, argocd-access-group, role:admin
