# documentation

## create sealed secret

```bash

kubectl create secret generic calibre-secret \
  --from-literal=calibre_password='bar' \
  --dry-run=client \
  -n calibre \
  -o yaml > temp_calibre_secret.yaml

kubeseal < temp_calibre_secret.yaml -o yaml >calibre-sealedsecret.yaml

```

