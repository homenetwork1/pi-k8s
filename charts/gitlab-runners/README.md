# documentation

## create sealed secret

```bash

kubectl create secret generic bvd-k8s-runners-registration-token \
  --from-literal=runner-registration-token='bar' \
  --from-literal=runner-token='' \
  --dry-run=client \
  -n gitlab-runners \
  -o yaml > temp_runner_secret.yaml

kubeseal < temp_runner_secret.yaml -o yaml >./charts/gitlab-runners/templates/registration-runner-sealedsecret.yaml

```
