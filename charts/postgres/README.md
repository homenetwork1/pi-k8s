# documentation

## create sealed secret

```bash
kubectl create secret generic postgresql-secrets \
  --from-literal=postgres-password='bar' \
  --from-literal=password='foo' \
  --dry-run=client \
  -n postgres \
  -o yaml > temp_runner_secret.yaml

kubeseal < temp_runner_secret.yaml -o yaml >./charts/postgres/templates/postgresql-sealedsecret.yaml
```
