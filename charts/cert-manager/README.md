# documentation

## install crd sepuratly from the helm chart

```bash
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.9.1/cert-manager.crds.yaml
```

## create sealed secret

```bash

kubectl create secret generic cloudflare-api-token-secret \
  --from-literal=api-token=<token> \
  --dry-run=client \
  -n cert-manager \
  -o yaml > temp_cloudflare_secret.yaml

kubeseal < temp_cloudflare_secret.yaml -o yaml > ./charts/cert-manager/templates/cloudflare-api-token-sealedsecret.yaml

```
